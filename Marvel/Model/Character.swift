//
//  Characters.swift
//  Marvel
//
//  Created by Juliano Terres on 17/01/18.
//  Copyright © 2018 Juliano Terres. All rights reserved.
//

import UIKit
import ObjectMapper

class Character: NSObject, Mappable {
    
    var id : Int?
    var name : String?
    var desc : String?
    var thumbnail : Thumbnail?
    var comics : Comics?
    var series : Series?
    var stories : Stories?
    var events : Events?
    
    override init() {}
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        desc <- map["description"]
        thumbnail <- map["thumbnail"]
        comics <- map["comics"]
        series <- map["series"]
        stories <- map["stories"]
        events <- map["events"]
    }
    
    required init(coder decoder: NSCoder) {
        self.id = decoder.decodeObject(forKey: "id") as? Int
        self.name = decoder.decodeObject(forKey: "name") as? String
        self.desc = decoder.decodeObject(forKey: "desc") as? String
        self.thumbnail = decoder.decodeObject(forKey: "thumbnail") as? Thumbnail
        self.comics = decoder.decodeObject(forKey: "comics") as? Comics
        self.series = decoder.decodeObject(forKey: "series") as? Series
        self.stories = decoder.decodeObject(forKey: "stories") as? Stories
        self.events = decoder.decodeObject(forKey: "events") as? Events
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.id, forKey: "id")
        coder.encode(self.name, forKey: "name")
        coder.encode(self.desc, forKey: "desc")
        coder.encode(self.thumbnail, forKey: "thumbnail")
        coder.encode(self.comics, forKey: "comics")
        coder.encode(self.series, forKey: "series")
        coder.encode(self.stories, forKey: "stories")
        coder.encode(self.events, forKey: "events")
    }
    
}
