//
//  Series.swift
//  Marvel
//
//  Created by Juliano Terres on 19/01/18.
//  Copyright © 2018 Juliano Terres. All rights reserved.
//

import UIKit
import ObjectMapper

class Series: NSObject, Mappable {
    
    var items : [Items]?
    
    override init() {}
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        items <- map["items"]
    }
    
    required init(coder decoder: NSCoder) {
        self.items = decoder.decodeObject(forKey: "items") as? [Items]
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.items, forKey: "items")
    }
    
}
