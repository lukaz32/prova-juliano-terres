//
//  Thumbnail.swift
//  Marvel
//
//  Created by Juliano Terres on 17/01/18.
//  Copyright © 2018 Juliano Terres. All rights reserved.
//

import UIKit
import ObjectMapper

class Thumbnail: NSObject, Mappable {

    var path : String?
    var typeFile : String?
    
    override init() {}
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        path <- map["path"]
        typeFile <- map["extension"]
    }
    
    required init(coder decoder: NSCoder) {
        self.path = decoder.decodeObject(forKey: "path") as? String
        self.typeFile = decoder.decodeObject(forKey: "typeFile") as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.path, forKey: "path")
        coder.encode(self.typeFile, forKey: "typeFile")
    }
    
}
