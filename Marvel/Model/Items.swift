//
//  Items.swift
//  Marvel
//
//  Created by Juliano Terres on 19/01/18.
//  Copyright © 2018 Juliano Terres. All rights reserved.
//

import UIKit
import ObjectMapper

class Items: NSObject, Mappable {
    
    var name : String?
    
    override init() {}
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        name <- map["name"]
    }
    
    required init(coder decoder: NSCoder) {
        self.name = decoder.decodeObject(forKey: "name") as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.name, forKey: "name")
    }
    
}
