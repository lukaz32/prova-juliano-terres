//
//  Characters.swift
//  Marvel
//
//  Created by Juliano Terres on 17/01/18.
//  Copyright © 2018 Juliano Terres. All rights reserved.
//

import UIKit
import ObjectMapper

class Character: NSObject, Mappable {

    var id : Int?
    var name : String?
    var desc : String?
    var thumbnail : Thumbnail?
    
    override init() {}
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        desc <- map["description"]
    }
    
    required init(coder decoder: NSCoder) {
        self.id = decoder.decodeObject(forKey: "id") as? Int
        self.name = decoder.decodeObject(forKey: "name") as? String
        self.desc = decoder.decodeObject(forKey: "desc") as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.id, forKey: "id")
        coder.encode(self.name, forKey: "name")
        coder.encode(self.desc, forKey: "desc")
    }
    
}
