//
//  CharactersListViewController.swift
//  Marvel
//
//  Created by Juliano Terres on 18/01/18.
//  Copyright © 2018 Juliano Terres. All rights reserved.
//

import UIKit

class CharactersListViewController: BaseViewController {
    // FIXME: ⚠️ Não é necessario explicitar o Type - Usar type inference
    let service : CharactersService = CharactersService()
    var characters : [Character] = []
    var offset : Int = 0
    var limit : Int = 10
    var loaderEnable : Bool = false
    
    @IBOutlet weak var uiLoaderInitial: UIActivityIndicatorView!
    @IBOutlet weak var uiLoaderMore: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.isHidden = true
        self.loadCharacters()
    }

    // FIXME: ⚠️ Remover métodos não utilizados
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadCharacters(){
        // FIXME: ⚠️ CharactersService não precisa ser uma classe
        self.service.getCharacters(offset: self.offset, limit: self.limit, completion: { (charactersApi) in
            // FIXME: ⚠️ Usar a capture list para capturar o self como weak dentro da closure
            self.characters.append(contentsOf: charactersApi)
            self.tableView.reloadData()
            self.tableView.isHidden = false
            self.uiLoaderInitial.stopAnimating()
            self.loaderEnable = false
            self.offset += self.limit
            
        }) { (error) in
            // FIXME: ⚠️ Tratar o erro adequadamente
        }
        
    }

}

extension CharactersListViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.characters.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : CharactersListCell = tableView.dequeueReusableCell(withIdentifier: "Default", for: indexPath) as! CharactersListCell
        cell.character = self.characters[indexPath.row]
        
        cell.mount()
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let controller : CharactersDetailViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CharactersDetailViewController") as! CharactersDetailViewController
        controller.character = self.characters[indexPath.row]
        
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if self.loaderEnable == false{
            self.uiLoaderMore.startAnimating()
            self.loadCharacters()
            self.loaderEnable = true
        }
        
    }
    
}
