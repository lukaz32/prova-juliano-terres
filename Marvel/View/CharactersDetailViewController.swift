//
//  CharactersDetailViewController.swift
//  Marvel
//
//  Created by Juliano Terres on 18/01/18.
//  Copyright © 2018 Juliano Terres. All rights reserved.
//

import UIKit

public enum CharactersDetailTableViewCellsSections {
    case Description
    case ComicsTitle
    case ComicsList
    case SeriesTitle
    case SeriesList
    case StoriesTitle
    case StoriesList
    case EventsTitle
    case EventsList
}

class CharactersDetailViewController: BaseViewController {

    var character : Character = Character()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.character.name
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension CharactersDetailViewController: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // FIXME: ⚠️ Utilizar um switch nesse caso
        if section == CharactersDetailTableViewCellsSections.ComicsTitle.hashValue{
            // FIXME: ⚠️ Evitar force-unwrapping desnecessários
            if (self.character.comics?.items?.count)! == 0{
                return 0
            }
        }
        
        if section == CharactersDetailTableViewCellsSections.ComicsList.hashValue{
            return (self.character.comics?.items?.count)!
        }
        
        if section == CharactersDetailTableViewCellsSections.SeriesTitle.hashValue{
            if (self.character.series?.items?.count)! == 0{
                return 0
            }
        }
        
        if section == CharactersDetailTableViewCellsSections.SeriesList.hashValue{
            return (self.character.series?.items?.count)!
        }
        
        if section == CharactersDetailTableViewCellsSections.StoriesTitle.hashValue{
            if (self.character.stories?.items?.count)! == 0{
                return 0
            }
        }
        
        if section == CharactersDetailTableViewCellsSections.StoriesList.hashValue{
            return (self.character.stories?.items?.count)!
        }
        
        if section == CharactersDetailTableViewCellsSections.EventsTitle.hashValue{
            if (self.character.events?.items?.count)! == 0{
                return 0
            }
        }
        
        if section == CharactersDetailTableViewCellsSections.EventsList.hashValue{
            return (self.character.events?.items?.count)!
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // FIXME: ⚠️ Utilizar um switch nesse caso
        if indexPath.section == CharactersDetailTableViewCellsSections.Description.hashValue{
            return 132
        }
        
        if indexPath.section == CharactersDetailTableViewCellsSections.ComicsTitle.hashValue ||
           indexPath.section == CharactersDetailTableViewCellsSections.SeriesTitle.hashValue ||
           indexPath.section == CharactersDetailTableViewCellsSections.StoriesTitle.hashValue ||
           indexPath.section == CharactersDetailTableViewCellsSections.EventsTitle.hashValue{
            return 37
        }
        
        if indexPath.section == CharactersDetailTableViewCellsSections.ComicsList.hashValue ||
           indexPath.section == CharactersDetailTableViewCellsSections.SeriesList.hashValue ||
           indexPath.section == CharactersDetailTableViewCellsSections.StoriesList.hashValue ||
           indexPath.section == CharactersDetailTableViewCellsSections.EventsList.hashValue{
            return 30
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : CharactersDetailCell = CharactersDetailCell()
        
        if indexPath.section == CharactersDetailTableViewCellsSections.Description.hashValue{
            // FIXME: ⚠️ Usar strong typed identifiers
            cell = tableView.dequeueReusableCell(withIdentifier: "Description", for: indexPath) as! CharactersDetailCell
        }
        
        if indexPath.section == CharactersDetailTableViewCellsSections.ComicsTitle.hashValue ||
           indexPath.section == CharactersDetailTableViewCellsSections.SeriesTitle.hashValue ||
           indexPath.section == CharactersDetailTableViewCellsSections.StoriesTitle.hashValue ||
           indexPath.section == CharactersDetailTableViewCellsSections.EventsTitle.hashValue{
            cell = tableView.dequeueReusableCell(withIdentifier: "Title", for: indexPath) as! CharactersDetailCell
        }
        
        if indexPath.section == CharactersDetailTableViewCellsSections.ComicsList.hashValue ||
           indexPath.section == CharactersDetailTableViewCellsSections.SeriesList.hashValue ||
           indexPath.section == CharactersDetailTableViewCellsSections.StoriesList.hashValue ||
           indexPath.section == CharactersDetailTableViewCellsSections.EventsList.hashValue{
            cell = tableView.dequeueReusableCell(withIdentifier: "ListItem", for: indexPath) as! CharactersDetailCell
        }
        
        cell.character = self.character
        cell.indexPath = indexPath
        cell.mount()
        
        return cell
        
    }
    
}
