//
//  BaseViewController.swift
//  Marvel
//
//  Created by Juliano Terres on 18/01/18.
//  Copyright © 2018 Juliano Terres. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBarBackButtonCustom()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func navigationBarBackButtonCustom(){
        // FIXME: ⚠️ Não é necessário explicitar o self aqui
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "back.png")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "back.png")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        
    }
    
}
