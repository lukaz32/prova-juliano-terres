//
//  CharactersService.swift
//  Marvel
//
//  Created by Juliano Terres on 17/01/18.
//  Copyright © 2018 Juliano Terres. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class CharactersService: NSObject {
    // FIXME: ⚠️ Essas properties deveria ser privadas
    let apiKey : String = "d89c919dbb61f12a1764b86bd8e8b73d"
    let ts : String = "abcdefghijlmanopq"
    let textHash : String = "da037efefed402e4a5f43a0a3a57c18e"
    
    func getCharacters(offset: Int, limit: Int, completion : @escaping(_ success: [Character]) -> Void, error : @escaping(_ error: String) -> Void){
        // FIXME: ⚠️ Essa base podia estar em uma constante
        let URL = "https://gateway.marvel.com/v1/public/characters?apikey="+self.apiKey+"&hash="+self.textHash+"&ts="+self.ts+"&offset="+String(offset)+"&limit="+String(limit)
        
        Alamofire.request(URL).responseJSON { (response) in
            // FIXME: ⚠️ Force unwrapping nesse caso é BEM perigoso
            let result = response.result.value! as? [String:Any]
            let retorno = result!["data"] as? [String:Any]
            
            let characters : [Character] = Mapper<Character>().mapArray(JSONArray: retorno!["results"] as! [[String : Any]])
            
            completion(characters)
            
        }
        
    }
    
}
