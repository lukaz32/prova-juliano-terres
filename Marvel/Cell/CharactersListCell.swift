//
//  ListCell.swift
//  Marvel
//
//  Created by Juliano Terres on 16/01/18.
//  Copyright © 2018 Juliano Terres. All rights reserved.
//

import UIKit
import Kingfisher

class CharactersListCell: UITableViewCell {

    var character : Character = Character()
    
    @IBOutlet weak var ivImage: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    // FIXME: ⚠️ Remover métodos não utilizados
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func mount(){
        
        self.lblName.text = self.character.name
        self.lblDescription.text = self.character.desc
        // FIXME: ⚠️ Preferir guard let for quick exiting & avoid hadouken code
        if let path = self.character.thumbnail?.path, let typeFile = self.character.thumbnail?.typeFile{
            
            if let url = URL(string: path+"/portrait_medium."+typeFile){
                self.ivImage.kf.indicatorType = .activity
                self.ivImage.kf.setImage(with: url)
            }
            
        }
        
    }

}
