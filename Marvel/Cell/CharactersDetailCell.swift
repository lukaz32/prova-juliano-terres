//
//  CharactersDetailCell.swift
//  Marvel
//
//  Created by Juliano Terres on 18/01/18.
//  Copyright © 2018 Juliano Terres. All rights reserved.
//

import UIKit

class CharactersDetailCell: UITableViewCell {

    var character : Character = Character()
    var indexPath : IndexPath = IndexPath()
    
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var ivImage: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblListItem: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func mount(){
        
        if self.indexPath.section == CharactersDetailTableViewCellsSections.Description.hashValue {
        
            if self.character.desc != "" {
                self.lblDescription.text = self.character.desc
            }else{
                self.lblDescription.text = "Não há descrição para este personagem."
            }
            
            
            if let path = self.character.thumbnail?.path, let typeFile = self.character.thumbnail?.typeFile{
                
                if let url = URL(string: path+"/standard_medium."+typeFile){
                    self.ivImage.kf.indicatorType = .activity
                    self.ivImage.kf.setImage(with: url)
                }
                
            }
            
        }
        
        if self.indexPath.section == CharactersDetailTableViewCellsSections.ComicsTitle.hashValue {
            self.lblTitle.text = "Lista de Comics"
        }
        
        if self.indexPath.section == CharactersDetailTableViewCellsSections.ComicsList.hashValue {
            if let item = self.character.comics?.items![indexPath.row]{
                self.lblListItem.text = "- "+item.name!
            }
        }
        
        if self.indexPath.section == CharactersDetailTableViewCellsSections.SeriesTitle.hashValue {
            self.lblTitle.text = "Lista de Séries"
        }
        
        if self.indexPath.section == CharactersDetailTableViewCellsSections.SeriesList.hashValue {
            if let item = self.character.series?.items![indexPath.row]{
                self.lblListItem.text = "- "+item.name!
            }
        }
        
        if self.indexPath.section == CharactersDetailTableViewCellsSections.StoriesTitle.hashValue {
            self.lblTitle.text = "Lista de Histórias"
        }
        
        if self.indexPath.section == CharactersDetailTableViewCellsSections.StoriesList.hashValue {
            if let item = self.character.stories?.items![indexPath.row]{
                self.lblListItem.text = "- "+item.name!
            }
        }
        
        if self.indexPath.section == CharactersDetailTableViewCellsSections.EventsTitle.hashValue {
            self.lblTitle.text = "Lista de Eventos"
        }
        
        if self.indexPath.section == CharactersDetailTableViewCellsSections.EventsList.hashValue {
            if let item = self.character.events?.items![indexPath.row]{
                self.lblListItem.text = "- "+item.name!
            }
        }
        
    }

}
